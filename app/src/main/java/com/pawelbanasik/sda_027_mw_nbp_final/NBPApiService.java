package com.pawelbanasik.sda_027_mw_nbp_final;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Pawel on 16.05.2017.
 */

public interface NBPApiService {

    @GET("exchangerates/tables/{table}")
    Call<List<CurrencyTable>> getTables(@Path("table") String table, @Query("format") String format);

}
