package com.pawelbanasik.sda_027_mw_nbp_final;

/**
 * Created by Pawel on 16.05.2017.
 */

public class CurrencyRate {

    private String currency;
    private String code;
    private String mid;

    public String getCurrency() {
        return currency;
    }

    public String getCode() {
        return code;
    }

    public String getMid() {
        return mid;
    }


}
