package com.pawelbanasik.sda_027_mw_nbp_final;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public class MainActivity extends AppCompatActivity implements CurrencyProvider.CurrencyAsyncTaskListener {

    private CurrencyAdapter currencyAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        currencyAdapter = new CurrencyAdapter();
        recyclerView.setAdapter(currencyAdapter);
        new CurrencyProvider(this).execute("A");

    }


    @Override
    public void onDataLoaded(CurrencyTable currencyTable) {
        currencyAdapter.setCurrencyRateList(currencyTable.getRates());

    }
}
