package com.pawelbanasik.sda_027_mw_nbp_final;

import java.util.List;

/**
 * Created by Pawel on 16.05.2017.
 */

public class CurrencyTable {

    private String table;
    private String no;
    private String effectiveDate;
    private List<CurrencyRate> rates;

    public String getTable() {
        return table;
    }

    public String getNo() {
        return no;
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public List<CurrencyRate> getRates() {
        return rates;
    }

}
