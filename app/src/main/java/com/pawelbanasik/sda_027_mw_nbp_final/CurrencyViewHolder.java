package com.pawelbanasik.sda_027_mw_nbp_final;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Pawel on 16.05.2017.
 */

public class CurrencyViewHolder extends RecyclerView.ViewHolder{

    private TextView currency;
    private TextView code;
    private TextView mid;


    public CurrencyViewHolder(View itemView) {
        super(itemView);
        currency = (TextView) itemView.findViewById(R.id.currency_text_view);
        code  = (TextView) itemView.findViewById(R.id.code_text_view);
        mid = (TextView) itemView.findViewById(R.id.mid_text_view);

    }

    public void setCurrency(String currency) {
        this.currency.setText(currency);

    }

    public void setCode(String code) {
        this.code.setText(code);
    }

    public void setMid(String mid) {
        this.mid.setText(mid);
    }
}
