package com.pawelbanasik.sda_027_mw_nbp_final;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Collections;
import java.util.List;

/**
 * Created by Pawel on 16.05.2017.
 */

public class CurrencyAdapter extends RecyclerView.Adapter<CurrencyViewHolder> {

    private List<CurrencyRate> currencyRateList = Collections.emptyList();

    @Override
    public CurrencyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_currency, parent, false);
        return new CurrencyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CurrencyViewHolder holder, int position) {
        CurrencyRate currencyRate = currencyRateList.get(position);
        holder.setCode(currencyRate.getCode());
        holder.setCurrency(currencyRate.getCurrency());
        holder.setMid(currencyRate.getMid());
    }

    @Override
    public int getItemCount() {
        return currencyRateList.size();
    }

    public void setCurrencyRateList(List<CurrencyRate> currencyRateList) {
        this.currencyRateList = currencyRateList;
        notifyDataSetChanged();
    }
}
