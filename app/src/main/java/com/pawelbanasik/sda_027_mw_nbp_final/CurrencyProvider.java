package com.pawelbanasik.sda_027_mw_nbp_final;

import android.os.AsyncTask;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Pawel on 16.05.2017.
 */

public class CurrencyProvider extends AsyncTask<String, Void, CurrencyTable> {

    private CurrencyAsyncTaskListener listener;
    NBPApiService nbpApiService;

    public CurrencyProvider(CurrencyAsyncTaskListener listener) {
        this.listener = listener;
        Retrofit retrofit = new Retrofit.Builder().baseUrl("http://api.nbp.pl/api/").addConverterFactory(GsonConverterFactory.create()).build();
        nbpApiService = retrofit.create(NBPApiService.class);
    }


    @Override
    public CurrencyTable doInBackground(String... params) {


        Call<List<CurrencyTable>> tablesCall = nbpApiService.getTables("A", "json");
        Response<List<CurrencyTable>> tablesResponse = null;
        try {
            tablesResponse = tablesCall.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<CurrencyTable> table;
        if (tablesResponse.isSuccessful()) {
            table = tablesResponse.body();

            return table.get(0);
        }
        return null;
    }

    @Override
    protected void onPostExecute(CurrencyTable result) {
        listener.onDataLoaded(result);
    }

    public interface CurrencyAsyncTaskListener {
        void onDataLoaded(CurrencyTable currencyTable);
    }


}
